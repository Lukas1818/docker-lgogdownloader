FROM debian:stable-slim

LABEL org.opencontainers.image.url="https://gitlab.com/Lukas1818/docker-lgogdownloader/container_registry"
LABEL org.opencontainers.image.title="noninteractive lgogdownloader Docker image with optimal schedule"
LABEL org.opencontainers.image.source="https://gitlab.com/Lukas1818/docker-lgogdownloader"

ENV DEBIAN_FRONTEND noninteractive

# Install lgogdownloader
RUN apt-get update \
 && apt-get install -y lgogdownloader expect

COPY login /usr/local/bin 
COPY start.sh /usr/local/bin
RUN chmod a+rx /usr/local/bin/login /usr/local/bin/start.sh \
 && chmod uga-w /usr/local/bin/login /usr/local/bin/start.sh

# Add user
RUN useradd -m -u 1000 user
USER user
WORKDIR /home/user/

ENTRYPOINT ["/usr/local/bin/start.sh"]
CMD ["--help"]
